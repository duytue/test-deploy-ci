#!/bin/bash

# any future command that fails will exit the script
set -e

# cd to repo
cd /home/ubuntu/test-deploy-ci

# clone the repo again
echo "Cloning source code"
git pull origin master
echo "Source code pulled successfully"

#install npm packages
echo "Running npm install"
npm install
echo "npm install done"

#Restart the node server
echo "Start server"
npm start